#include <Adafruit_NeoPixel.h>
#include <IRLibDecodeBase.h>
#include <IRLib_P01_NEC.h>
#include <IRLibCombo.h>

IRdecode myDecoder;

#include <IRLibRecvPCI.h>
IRrecvPCI myReceiver(2);

Adafruit_NeoPixel strip = Adafruit_NeoPixel(3,7,NEO_RGB+NEO_KHZ800);

#define NUMBER_OF_FLAMES 1 // depends on number of neopixel triplets. 5 for 16 NeoPixel ring. 4 for 12 NeoPixel ring
#define FLICKER_CHANCE 3 // increase this to increase the chances an individual flame will flicker

uint32_t rez_range = 256 * 3;
#define D_ false

// console buttons:
struct flame_element {
  int brightness;
  int step;
  int max_brightness;
  long rgb[3];
  byte state;
} flames[1];

int new_brightness = 0;
unsigned long rgb[3]; //reusable temporary array
uint8_t scaleD_rgb[3];
byte acc;

#define RED_TABLE 0
#define BLUE_TABLE 1
#define GREEN_TABLE 2
#define YELLOW_TABLE 3
int color_table = BLUE_TABLE;  // Start with blue flames

#define SCALERVAL 256*3

const int coloredflames[4][22][3] = {
  {
    { SCALERVAL, 0,  0}, // Red
    { SCALERVAL, 0,  0},
    { SCALERVAL, 0,  0},
    { SCALERVAL, 0,  0},
    { SCALERVAL, 0,  0},
    { SCALERVAL, 0,  0},
    { SCALERVAL, 0,  0},
    { SCALERVAL, 0,  0},
    { SCALERVAL, SCALERVAL * .4,  0},  // Orange
    { SCALERVAL, SCALERVAL * .4,  0},
    { SCALERVAL, SCALERVAL * .4,  0},
    { SCALERVAL, SCALERVAL * .4,  0},
    { SCALERVAL, SCALERVAL * .3,  0},  // Red Orange
    { SCALERVAL, SCALERVAL * .3,  0},
    { SCALERVAL, SCALERVAL * .3,  0},
    { SCALERVAL, SCALERVAL * .3,  0},
    { SCALERVAL, SCALERVAL * .3,  0},
    { SCALERVAL, SCALERVAL * .3,  0},
    { SCALERVAL, SCALERVAL * .3,  0},
    { SCALERVAL, SCALERVAL * .3,  SCALERVAL}, // white
    { 0, SCALERVAL * .2,  SCALERVAL}, // that one blue flame
    { SCALERVAL,  SCALERVAL * .3,  SCALERVAL * .5}  // Pink
  },
  {
    { 0, 0, SCALERVAL}, // Blue
    { 0, 0, SCALERVAL},
    { 0, 0, SCALERVAL},
    { 0, 0, SCALERVAL},
    { 0, 0, SCALERVAL},
    { 0, 0, SCALERVAL},
    { 0, 0, SCALERVAL},
    { 0, 0, SCALERVAL},
    { SCALERVAL * .6, 0,  SCALERVAL * .6}, // Purple
    { SCALERVAL * .6, 0,  SCALERVAL * .6},
    { SCALERVAL * .6, 0,  SCALERVAL * .6},
    { SCALERVAL * .6, 0,  SCALERVAL * .6},
    { SCALERVAL * .3, 0,  SCALERVAL * .3}, // Dark Purple
    { SCALERVAL * .3, 0,  SCALERVAL * .3},
    { SCALERVAL * .3, 0,  SCALERVAL * .3},
    { SCALERVAL * .3, 0,  SCALERVAL * .3},
    { SCALERVAL * .3, 0,  SCALERVAL * .3},
    { SCALERVAL * .3, 0,  SCALERVAL * .3},
    { SCALERVAL * .3, 0,  SCALERVAL * .3},
    { SCALERVAL, SCALERVAL * .3,  SCALERVAL}, // white
    { 0, SCALERVAL * .2,  SCALERVAL}, // that one blue flame
    { SCALERVAL,  SCALERVAL * .3,  SCALERVAL * .5} // Pink
  }, {
    { 0, SCALERVAL, 0}, // Green
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL, 0},
    { 0, SCALERVAL,  SCALERVAL * .2}, // Light Green
    { 0, SCALERVAL,  SCALERVAL * .2},
    { 0, SCALERVAL,  SCALERVAL * .2},
    { 0, SCALERVAL,  SCALERVAL * .2},
    { 0, SCALERVAL * .6,  SCALERVAL * .3}, // Dark Green
    { 0, SCALERVAL * .6,  SCALERVAL * .3},
    { 0, SCALERVAL * .6,  SCALERVAL * .3},
    { 0, SCALERVAL * .6,  SCALERVAL * .3},
    { 0, SCALERVAL * .6,  SCALERVAL * .3},
    { 0, SCALERVAL * .6,  SCALERVAL * .3},
    { 0, SCALERVAL * .6,  SCALERVAL * .3},
    { SCALERVAL * .3, SCALERVAL,  SCALERVAL}, // white
    { 0, SCALERVAL * .2,  SCALERVAL}, // that one blue flame
    { SCALERVAL * .6,  SCALERVAL,  0} // light green
  },{
    { SCALERVAL, SCALERVAL, 0}, // Yellow
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL, 0},
    { SCALERVAL, SCALERVAL,  SCALERVAL * .7}, // Light Yellow
    { SCALERVAL, SCALERVAL,  SCALERVAL * .7},
    { SCALERVAL, SCALERVAL,  SCALERVAL * .7},
    { SCALERVAL, SCALERVAL,  SCALERVAL * .7},
    { SCALERVAL * .8, SCALERVAL * .8,  }, // Darker Yellow
    { SCALERVAL * .8, SCALERVAL * .8,  0},
    { SCALERVAL * .8, SCALERVAL * .8,  0},
    { SCALERVAL * .8, SCALERVAL * .8,  0},
    { SCALERVAL * .8, SCALERVAL * .8,  0},
    { SCALERVAL * .8, SCALERVAL * .8,  0},
    { SCALERVAL * .8, SCALERVAL * .8,  0},
    { SCALERVAL * .3, SCALERVAL,  SCALERVAL}, // white
    { 0, SCALERVAL * .2,  SCALERVAL}, // that one blue flame
    { SCALERVAL,  SCALERVAL, SCALERVAL * .7} // light yellow
  }
  
};

// 22 milliseconds
#define FLAME_DELAY 22

#define RED_SIGNAL 0xffd12e
#define GREEN_SIGNAL 0xff51ae
#define YELLOW_SIGNAL 0xff827d
#define BLUE_SIGNAL 0xffa25d

void setup() {
  Serial.begin(9600);
  delay(2000); while(!Serial);
  myReceiver.enableIRIn();
  strip.begin();
  strip.show();
  randomSeed(analogRead(6));
  InitFlames();
}

void loop() {

  processFlames();
  strip.show();
  delay(FLAME_DELAY);
  
  if(myReceiver.getResults()){
    if(myDecoder.decode()){
      Serial.println(F("Received IR"));
      
      switch(myDecoder.value){
        case RED_SIGNAL:
          Serial.println(F("Red")); 
          color_table = RED_TABLE; 
          break;
        case BLUE_SIGNAL: 
          Serial.println(F("Blue"));
          color_table = BLUE_TABLE; 
          break;
        case GREEN_SIGNAL: 
          Serial.println(F("Green"));
          color_table = GREEN_TABLE; 
          break;
        case YELLOW_SIGNAL: 
          Serial.println(F("Yellow"));
          color_table = YELLOW_TABLE; 
          break;
       // default: color_table = BLUE_TABLE; break;  
      }
      
      clear();
      InitFlames();
    }
    myReceiver.enableIRIn();
  }

 
}

void processFlames() {
  //Serial.println(F("processFlames()"));
  for (byte flame_count = 0; flame_count < NUMBER_OF_FLAMES; flame_count++) {
    switch (flames[flame_count].state) {
      case 0: // reset
        CreateNewFlame(flame_count,color_table);
        break;

      case 1: //increasing
        new_brightness = flames[flame_count].brightness + flames[flame_count].step;
        if (new_brightness > flames[flame_count].max_brightness) {
          UpdateFlameColor(flame_count, flames[flame_count].max_brightness);
          flames[flame_count].brightness = flames[flame_count].max_brightness;
          flames[flame_count].step = GetStepSize(); // pick a different speed for flame going out
          flames[flame_count].state = 2;
        } else {
          UpdateFlameColor(flame_count, new_brightness);
          flames[flame_count].brightness = new_brightness;
        }

        break;


      case 2: //decreasing
        new_brightness = flames[flame_count].brightness - flames[flame_count].step;
        // chance to flicker/rekindle:
        if (random(new_brightness) < FLICKER_CHANCE) {
          // rekindle:
          flames[flame_count].state = 1; //increase again
          flames[flame_count].brightness = max(GetMaxBrightness(), flames[flame_count].brightness);
          flames[flame_count].step = GetStepSize();

        } else {
          if (new_brightness < 1) {
            flames[flame_count].state = 0; // bottomed out - reset to next flame
            flames[flame_count].brightness = 0;
            UpdateFlameColor(flame_count, 0);
          } else {
            UpdateFlameColor(flame_count, new_brightness);
            flames[flame_count].brightness = new_brightness;
          }
        }
        break;
    }

  }
  
}


void InitFlames() {
  // Sets initial states in flames array
  for (byte i = 0; i < NUMBER_OF_FLAMES; i++) {
    flames[i].state = 0;

  }
}


void UpdateFlameColor(byte flame_num, int new_brightness) {
  //
  uint32_t c = 0;
  uint32_t color_channel_value;
  byte rgb_channel;

  new_brightness = min(new_brightness, flames[flame_num].max_brightness);


  for (byte rgb_channel = 0; rgb_channel < 3; rgb_channel++) {
    color_channel_value = flames[flame_num].rgb[rgb_channel];
    color_channel_value = color_channel_value * (uint32_t)new_brightness; // keep it long
    color_channel_value = color_channel_value / (uint32_t)rez_range;
    rgb[rgb_channel] = max(0L, color_channel_value);
  } // step through R G B



  // spread possible values of 0 -768 across 3 pixels
  for (byte sub_pixel = 0; sub_pixel < 3; sub_pixel++) {
    for (byte i = 0; i < 3; i++) { // rgb
      acc = rgb[i] / 3;
      byte d = rgb[i] % 3;
      if (sub_pixel < d) {
        acc++;
      }
      scaleD_rgb[i] = acc;

    }

    strip.setPixelColor(flame_num * 3 + sub_pixel, scaleD_rgb[0], scaleD_rgb[1], scaleD_rgb[2]);
  }

}


void CreateNewFlame(byte flame_num,int colortable) {
  flames[flame_num].step = GetStepSize();
  flames[flame_num].max_brightness = GetMaxBrightness();

  flames[flame_num].brightness = 0;
  flames[flame_num].state = 1;
  byte color_index = random(22);
  for (byte i = 0; i < 3; i++) {
    flames[flame_num].rgb[i] = coloredflames[colortable][color_index][i];
  }

}

int GetStepSize() {
  return random(70) + 1;
}
int GetMaxBrightness() {
  int retVal;
  //  retVal = random(rez_range/4) +  random(rez_range/4) + random(rez_range/4) + rez_range/4 +1; // bell curve
  //  retVal = random(rez_range*3/4) +  rez_range/4; // flat distribution
  retVal = random(rez_range / 2) +  rez_range / 2; // brighter flat distribution
  return retVal;
}

void clear() {

  for(byte i=0;i<3;i++){
    strip.setPixelColor(i,0,0,0);
  }
}



